import React, { Component } from 'react'
import { View, Text, Button, Alert, StyleSheet } from 'react-native'

class Screen2 extends Component {
    state = {
        username: this.props.location.state.username
    }

    goToScreen1 = () => {
        //send state back to screen 1
        this.props.history.push('/screen1', { myNumber: 20 })
        
    }

    UNSAFE_componentWillMount() {
        console.log(this.props)
    }
    render() {
        return (
            <View style={styles.container}>
            <View style={[styles.header, styles.row]}>
                <Text style={{color: 'white', fontSize: 25}}>Back</Text>
                <Text style={{color: 'white', fontSize: 25}}>profile page</Text>
            </View>
                
                <Text style={{color: 'white'}}>{this.state.username}</Text>
                <Button title="GO GO" onPress={this.goToScreen1}/>
            </View>
        )
    }
}

const styles = StyleSheet.create({

    container : {
        backgroundColor: 'grey',
        flex: 1
    },
    header : {
        
    },
    row : {
        
    }
})

export default Screen2