import React, { Component } from 'react'
import { View, Text, Alert, StyleSheet, Image, TextInput, TouchableOpacity } from 'react-native'
import { Link } from 'react-router-native'
class Screen1 extends Component {

    state = {
        username: '',
        password: ''
    } 
    

    // UNSAFE_componentWillMount() {
    //     console.log(this.props)

    //     //ถ้าในพรอพมีโลเคชั่น และ ในโลเคชั่นมีสเตท และในสเตทมีมายนัมเบอ แล้วให้ alert
    //     if(this.props.location && this.props.location.state && this.props.location.state.myNumber){
    //         Alert.alert('your number is', this.props.location.state.myNumber+'')
    //     }
    // }

    login = () => {
        this.props.history.push('/screen2', {username: this.state.username, password:this.state.password})
    }
    render() {
        return (
          <View style={styles.container}>
            <View style={[styles.content, styles.center]}>
              <View style={[styles.image, styles.center]}>
                <Image style={styles.image} source={{ uri: 'https://www.dictionary.com/e/wp-content/uploads/2018/05/BTS-300x300.jpg' }} />
              </View>
            </View>
    
    
            <View style={styles.content}>
              <Text style={{alignItems: 'center'}}>{this.state.username} - {this.state.password}</Text>
              <View>
                <TextInput onChangeText={(value) => {this.setState({username:value})}} value={this.state.username} style={styles.textInput} placeholder='Username'/>
              </View>
    
              <View>
                <TextInput onChangeText={(value) => {this.setState({password:value})}} value={this.state.password} style={styles.textInput} placeholder='Password'/>
              </View>
    
    
              <TouchableOpacity onPress={this.login}>
                <View style={styles.touchable}>
                  <Text>Login</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        );
      }
    
}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: 'grey'
    },
    image: {
      alignItems: 'center',
      backgroundColor: 'white',
      width: 200,
      height: 200,
      borderRadius: 100
  
    },
    content: {
      flex: 1,
      flexDirection: 'column',
      justifyContent: 'center'
    },
    textInput: {
      margin: 8,
      alignItems: 'center',
      backgroundColor: 'white',
      height: 50
    },
    touchable: {
      alignItems: 'center',
      padding: 20,
      backgroundColor: 'darkgrey',
      margin: 14,
  
    },
    center: {
      justifyContent: 'center',
      alignItems: 'center'
    }
  
  
  });
  

export default Screen1